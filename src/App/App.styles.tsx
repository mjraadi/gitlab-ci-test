import styled from 'styled-components';
import { primaryColor } from 'styles/globalVariables';

export const AppContainer = styled.div`
  background-color: ${primaryColor};
  min-height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
  font-size: calc(10px + 2vmin);
  color: white;
  text-align: center;
`;