import React, { Fragment } from 'react';
import GlobalStyles from 'styles/globalStyles';
import Header from 'components/header/Header';
import { AppContainer } from './App.styles';
import { ThemeProvider } from 'styled-components';
import Heading from 'components/heading/Heading';
import * as globalVariables from 'styles/globalVariables';

const App: React.FC = () => {
  return (
    <AppContainer>
      <GlobalStyles />
      <ThemeProvider theme={{...globalVariables}}>
        <Fragment>
          <Heading 
            headingText="React Data Table Widget" 
            headingType="secondary"
          />
          <Header headerText={ "React TypeScript Test App" }/>
        </Fragment>
      </ThemeProvider>
    </AppContainer>
  );
};

export default App;
