import React from 'react';
import { IHeader } from './Header.interface';

const Header: React.FC<IHeader> = (props) => {
  return (
    <header>
      <p> { props.headerText } </p>
    </header>
  );
}
export default Header;