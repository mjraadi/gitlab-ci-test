import React from "react";
import { shallow, ShallowWrapper } from 'enzyme';
import Header from './Header';

/**
 * Initialize common properties to be passed
 * @param {*} props properies to be override
 */
function createTestProps(props = {}) {
  return {
    headerText: 'test',
    ...props,
  };
}

let wrapper:ShallowWrapper, props;

beforeEach(() => {
  props = createTestProps();
  wrapper = shallow(<Header {...props} /> );
});

describe('<Header /> Rendering', () => {
  it('Should render an <p> element with a text inside it', () => {
    expect(wrapper.text().trim()).toBe('test');
  });
});