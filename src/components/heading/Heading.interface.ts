export interface IHeading {
  /**
 * Set this to change heading type
 * @default primary
 */
  headingType?: 'primary' | 'secondary',
  headingText: string,
};