import styled from 'styled-components';

const HeadingBase = styled.h1`
  display: block;
  font-style: italic;
  margin-bottom: calc(.3 * ${props => props.theme.defaultFontSize});
  font-weight: 700;
`;

const HeadingPrimary = styled(HeadingBase)`
  color: ${props => props.theme.primaryColor};
  font-size: calc(2 * ${props => props.theme.defaultFontSize});
`;

const HeadingSecondary = styled(HeadingBase)`
  color: ${props => props.theme.defaultWhiteColor};
  font-size: calc(1.5 * ${props => props.theme.defaultFontSize});
`;

/*
* Finding a styled component using enzyme’s .find() can be a bit awkward
* To find the h1 we need to access the h1 as ‘Styled(H1)’.
* To solve this problem and make the test more readable we can directly set the
* displayName of the styled component to a set name which does not depend on the 
* implementation of the component:
* HeadingPrimary.displayName = 'HeadingPrimary';
* findable with wrapper.find('HeadingPrimary');
*/
HeadingPrimary.displayName = 'HeadingPrimary';
HeadingSecondary.displayName = 'HeadingSecondary';

export {
  HeadingPrimary,
  HeadingSecondary
};