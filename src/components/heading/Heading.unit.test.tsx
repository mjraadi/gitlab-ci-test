import React from "react";
import { shallow, ShallowWrapper } from 'enzyme';
import Heading from './Heading';
import { IHeading } from './Heading.interface';
/**
 * Initialize common properties to be passed
 * @param {*} props properies to be override
 */
function createTestProps(props = {}) {
  return {
    headingText: 'HeadingText',
    ...props,
  };
}

let wrapper:ShallowWrapper, props: IHeading;

beforeEach(() => {
  props = createTestProps();
  wrapper = shallow(<Heading {...props} /> );
});

describe('<Heading /> Rendering', () => {
  it('Should render an <h1> element without passing a headingType prop', () => {
    expect(wrapper.html().includes('h1')).toBeTruthy();
  });

  it('Should render an <h1> element by passing a headingType prop', () => {
    wrapper = shallow(<Heading {...props} headingType="primary" /> );
    expect(wrapper.html().includes('h1')).toBeTruthy();
  });

  it('Should render an <h1> element with some text inside it', () => {
    expect(wrapper.text().trim()).toBe('HeadingText');
  });

  it('Should render an <h1> element', () => {
    expect(wrapper.find('HeadingPrimary')).toHaveLength(1);
  });

  it('Should render an <h3> element by passing a headingType prop', () => {
    wrapper = shallow(<Heading {...props} headingType="secondary" /> );
    expect(wrapper.html().includes('h3')).toBeTruthy();
  });

  it('Should render an <h3> element with some text inside it', () => {
    wrapper = shallow(<Heading {...props} headingType="secondary" /> );
    expect(wrapper.text().trim()).toBe('HeadingText');
  });

});