import React from 'react';
import { IHeading } from './Heading.interface';
import { 
  HeadingPrimary,
  HeadingSecondary
} from './Headings.styles';

const Heading: React.FC<IHeading> = (props) => {
  switch (props.headingType) {
    case 'secondary':
      return (
        <HeadingSecondary as="h3" > 
          { props.headingText } 
        </HeadingSecondary>
      );
    case 'primary':
    default:
      return <HeadingPrimary as="h1" > { props.headingText } </HeadingPrimary>;
  }
};

// type headingType = 'primary' | 'secondary';
// type Kinds = Record<Kind, string>

Heading.defaultProps = {
  headingType: 'primary',
  headingText: '',
};

export default Heading;