import { createGlobalStyle } from 'styled-components';
import {
  accentColor,
  primaryColor, 
  primaryFontFamily,
  defaultFontSize,
  defaultGrayColor, 
} from './globalVariables';

const globalStyles = createGlobalStyle`
html {
  box-sizing: border-box;
  font-size: calc(0.6em + 1vw);
}

*,
*::before,
*::after {
  box-sizing: inherit;
}
body {
  margin: 0;
  font-family: ${primaryFontFamily};
  font-size: ${defaultFontSize};
  font-weight: 400;
  line-height: 1.7;
}
p {
  font-size: ${defaultFontSize};
  color: ${defaultGrayColor}
}
::selection {
  background-color: ${accentColor};
  color: ${primaryColor};
}
`;

export default globalStyles;