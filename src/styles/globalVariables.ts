
// Colors
export const primaryColor = '#1A3347';
export const accentColor = '#07FF87';

// Shades of Gray
export const lightGrayColor = '#EEEEEE';
export const defaultGrayColor = '#DBDFE2';
export const darkGrayColor = '#CCCCCC';

// Shades of Black
export const darkBlackColor = '#102D3C';
export const defaultBlackColor = '#000';

// Shades of White
export const defaultWhiteColor = '#FFF';

// Match Results Indicator Colors
export const winIndicatorColor = accentColor;
export const loseIndicatorColor = '#FF7A7B';
export const drawIndicatorColor = '#FFF68D';

// Fonts
export const primaryFontFamily = "monsetrat, sansserif";
export const defaultFontSize = '1rem';