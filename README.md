This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Important Note
Make sure you have `yarn` installed in order to install the project dependencies. This project uses the `resolution` feature which is only supported by `yarn`.

## Available Scripts

In the project directory, you can run:

### `yarn`

Installs all the dependencies.

### `yarn start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn test:debug`

Launches the test runner in the interactive watch mode with node debugging enabled.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn:docz dev`

Runs the [`Docz`](https://github.com/pedronauck/docz) development server. This library is used for the project's documentations. `Docz` uses `MDX` as a standard format because of the flexibility of writing `JSX` or `TSX` inside markdown files.

### `yarn:docz build`

Generates all static files for you in the `.docz/dist` folder to deploy the documentation anywhere we want.

### `yarn build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## E2E Testing

This project uses `cypress` for its end-to-end testing. Here are a few reasons to why we use it:

* Isolated installation possible.
* Ships with TypeScript definitions out of the box.
Provides a nice interactive google chrome debug experience. This is very similar to how UI devs mostly work manually.
* Provides the ability to mock out and observe backend XHRs easily without changing your application code.

I created a separate `e2e` folder with its dependencies and scripts to isolate the app and my `e2e` tests. Here are a few reasons for creating a separate `e2e` folder especially for `cypress`:

* Creating a separate directory or `e2e` makes it easier to isolate its `package.json` dependencies from the rest of your project. This results in less dependency conflicts.
* Testing frameworks have a habit of polluting the global namespace with stuff like `describe` `it` `expect`. It is best to keep the e2e `tsconfig.json` and `node_modules` in this special `e2e` folder to prevent global type definition conflicts.

### Running `e2e` Tests

Before running the tests, please make sure the app is running. By default the app should run on `http://localhost:3000`. The tests will fail if the app is not running. You can run the tests by going to the `e2e` directory run the following scripts.

### `yarn cypress:gui`

Runs the tests in IDE mode.

### `yarn cypress:cli`

Runs the tests in CI mode.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
